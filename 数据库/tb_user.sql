/*
Navicat MariaDB Data Transfer

Source Server         : localhost_3306
Source Server Version : 100410
Source Host           : localhost:3306
Source Database       : tb_user

Target Server Type    : MariaDB
Target Server Version : 100410
File Encoding         : 65001

Date: 2020-11-09 20:38:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(20) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `ad` varchar(255) DEFAULT NULL,
  `score` int(255) DEFAULT NULL,
  `score1` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', '张三', '计算机系', '99', '0');
INSERT INTO `tb_user` VALUES ('2', '李四', '安全工程系', '98', '0');
INSERT INTO `tb_user` VALUES ('3', '王五', '外国语系', '97', '0');
INSERT INTO `tb_user` VALUES ('5', '钱一', '环境资源管理系', '96', '0');
INSERT INTO `tb_user` VALUES ('6', '张一', '计算机系', '95', '0');
INSERT INTO `tb_user` VALUES ('7', '苏大强', '经管系', '86', '0');
INSERT INTO `tb_user` VALUES ('8', '吴耀军', '计算机系', '76', '0');
